from django.conf.urls import patterns, include, url

from django.contrib import admin

from ControlPanel.views import NoiseData, ImpConfig

urlpatterns = patterns('',
    url(r'noise/data', NoiseData.as_view(),name="noise-data"),
    url(r'imp/config', ImpConfig.as_view(),name="imp-config"),
    )