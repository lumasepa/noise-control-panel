from django.conf.urls import patterns, url

from ControlPanel.admin_views import GraphicPanelView

urlpatterns = patterns('',
    url(r'^graphics-panel/$', GraphicPanelView.as_view(), name="graphics-panel"),
)